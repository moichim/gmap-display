const webpack = require("webpack");
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  "entry": {
    "index": path.resolve(__dirname, './src/index.js'),
  },
  "output": {
    path: path.resolve(__dirname, './assets'),
    filename: 'gdm.min.js',
  },
  "module": {
    "rules": [
		// JSX loader
      	{
        	"test": /\.jsx?$/,
			"loader": "babel-loader",
			options: {
				// Here you should change 'env' to '@babel/preset-env'
				presets: ['@babel/preset-env'],
			  },
      	},

		// CSS, PostCSS, and Sass
		{
			test: /\.(scss|css)$/,
			use: [
				MiniCssExtractPlugin.loader,
				'css-loader',
				'postcss-loader',
				'sass-loader'
			],
		},
    ]
  },
  "plugins": [
	new CleanWebpackPlugin(),
	new MiniCssExtractPlugin({
		filename: "gdm.min.css"
	}),
	new webpack.ProvidePlugin({
		$: "jquery",
		jQuery: "jquery"
	  })
  ],
  externals: {
	jquery: 'jQuery',
  }
}
