<?php
/**
 * Plugin Name:     Gmap Display
 * Plugin URI:      grafique.cz
 * Description:     Display posts on google map, along with filtering.
 * Author:          Jan Jáchim
 * Author URI:      jachim.grafique.cz
 * Text Domain:     gmap-display
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * GitHub Plugin URI: https://gitlab.com/moichim/gmap-display
 *
 * @package         Gmap_Display
 */

/**
 * This is just a sample plugin.
 */
function sample_plugin() {
	echo 'Do nothing';
}

// Constants

// Plugin version
define( 'GDM_VERSION', "1.2" );

// Plugin paths
define( 'GDM_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'GDM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Google Maps API key
define( 'GDM_KEY', "AIzaSyB0G1RBP4N2khGZVZidj5LX6n646oP7Upw" );

define( 'GDM_LABEL', "třídit podle:" );





// Custom post type
define( "GDM_CPT_SLUG" , "realita" ); // can never be translated
define( "GDM_CPT_NAME" , "Realita" );
define( "GDM_CPT_GENITIVE", "reality" );
define( "GDM_CPT_NAME_PL" , "Reality" );
define( "GDM_CPT_ICON" , "dashicons-admin-site-alt" );


// Generic taxonomies

// Flags serve for display purposes only
define( 'GDM_FLAGS', false );

// Icons serve for selection of predefined map markers
define( 'GDM_ICONS', false );

// Scale of the markers everywhere
define( 'GDM_ICON_X', 40 );
define( 'GDM_ICON_Y', 40 );



// Filtering taxonomies

// Setup taxonomies for filtering
define( "GDM_FILTERING", array(

	"locality" => array(

		// Taxonomy is hierarchical
		"hierarchical" => true,

		// Taxonomy has image field that shall
		// be displayed in filter
		"icon" => false,

		// Taxonomy name
		"label" => "Lokalita",

		// Taxonomy name in plural
		"label_pl" => "Lokality",

		// Taxonomy description
		"description" => "Popiska této taxonomie",

		// The filter displays inline
		"displays_inline" => false,

		// Probable obsolete properties
		"has_multiple" => true,
		"replaces_default" => false,
	),

	"type" => array(
		"hierarchical" => true,
		"icon" => false,
		"label" => "Typ plochy",
		"label_pl" => "Typy ploch",
		"description" => "Popiska taxonomie typů",
		"displays_inline" => false,
		"has_multiple" => false,
		"replaces_default" => false
	),
	"area" => array(
		"hierarchical" => true,
		"icon" => false,
		"label" => "Plocha k dispozici",
		"label_pl" => "Plochy k dispozici",
		"description" => "Plochy k dispozici",
		"displays_inline" => false,
		"has_multiple" => false,
		"replaces_default" => false
	),

) );

/**
 * Custom post meta that shall be passed into the
 * JSON objects and into templates
 * @see GDM_Manager::loadContent
 */
$fields = array(
	"post_thumbnail", "area_exact", "gdm_address_def","list_mark", "new", "lent"
);
define("GDM_CPT_QUERY_FIELDS", $fields);

/**
 * Which taxonomies
 */
define("GDM_CPT_QUERY_TAXONOMIES", array("type","area") );


// Templates

// Look for templates that come from plugin
define( "GDM_PLUGIN_TEMPLATES" , true );


// Default thumbnail
define( "GDM_THUMB_DEFAULT",  GDM_PLUGIN_URL . "gdm-thumbnail-default.svg"  );




// Includes

// Dependencies
require_once( GDM_PLUGIN_PATH . 'lib/dependencies.php' );

// Definitions
require_once( GDM_PLUGIN_PATH . 'lib/definitions.php' );

// Manager class
require_once( GDM_PLUGIN_PATH . 'lib/enqueue.php' );

// Manager class
require_once( GDM_PLUGIN_PATH . 'lib/manager.php' );

// Shortcode
require_once( GDM_PLUGIN_PATH . 'lib/shortcode.php' );

// Inclusion function
require_once( GDM_PLUGIN_PATH . 'lib/gdm.php' );

// CPT template routing
require_once( GDM_PLUGIN_PATH . 'lib/templates.php' );

// Setup ACF Google Maps Key
function gdm_acf_init() {
    acf_update_setting( 'google_api_key', GDM_KEY );
}
add_action( 'acf/init', 'gdm_acf_init' );
