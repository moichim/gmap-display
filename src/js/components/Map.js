import MapBase from "./MapBase";


/**
 * The map within lists
 */
export default class Map extends MapBase {

	constructor(controller, $element){

		super( $element );

		this.controller = controller;

		// This holds the array of current items on the map
		this.items = [];


	}

	debug( $input, $label=false ){
		if ( $label ) {
			console.log($label);
		}
		console.log($input);
	}

	markerString( marker ){
		let output = "<div style='text-align: center;padding: 0; margin: 0;'>";
		output += "<div style='width: 100%;'><strong>";
		output += marker.name;
		output += "</strong></div>";
		if ( marker.marker_taxonomies.length > 0 ) {

			let outBuf = [];

			marker.marker_taxonomies.map(function( scope ){

				if ( scope.length > 0 ) {
					let taxText = "<span>" + scope.join() + "</span>";
					outBuf.push( taxText );
				}

			});
			output += "<div class='epp-color__primary' style='width: 100%;'>";
			output += outBuf.join("<span style='display:inline-block;transform: scale(2.5);padding: 0 .5rem;'>&bull;</span>");
			output += "</div>";

		}

		output += "</div>";

		return output;
	}

	/**
	 * Update the contents on the map based on
	 * list of provided Content objects
	 * @param {Content} $contents
	 */
	updateItems( items = [] ){

		// Copy IDs of new items
		let itemsNew = items.map(item=>item.ID);

		// Copy IDs of old items
		let itemsOld = this.items.map(item=>item.ID);

		// Get the intersection of new and old IDs
		let intersection = itemsOld.filter(x => itemsNew.includes(x));

		// TO ADD = new - intersetion
		let itemsToAdd = itemsNew.filter(i=>!intersection.includes(i));

		// TO REMOVE = intersection - old
		let itemsToRemove = itemsOld.filter(i=>!intersection.includes(i));

		// Add markers to be added
		let map = this.map;

		let thisController = this;

		items.map(item=>{
			if ( itemsToAdd.includes( item.ID ) ) {
				let options = {
					position: {
						lat: parseFloat( item.lat ),
						lng: parseFloat( item.lon )
					},
					ID: item.ID,
					title: "Přiblížit",
					name: item.post_title,
					map: map,
					animation: google.maps.Animation.DROP,
					permalink: item.permalink,
					goes: false,
					marker_taxonomies: item.marker_taxonomies,
				};
				options.marker_taxonomies_text = thisController.markerString( options );

				if ( item.icon ) {
					/*
					options.optimised = false;
					options.icon = {
						url: item.icon,
						scaledSize: new google.maps.Size(40,40),
					}
					*/
					options.icon = item.icon;
				}

				let marker = new google.maps.Marker( options );


				let mapObject = map;

				marker.bubble = new google.maps.InfoWindow({
					content: marker.marker_taxonomies_text,
					disableAutoPan: true,
					minWidth: 150
				});
				marker.focused = false;

				// Opening and closing infowindows
				let mapController = this;
				marker.addListener("click", () => {
					mapController.handleInfoWindows(marker.ID);
				});

				// Marker infowindow on hover
				marker.addListener("mouseover",()=>{

					mapController.handleInfoWindows(marker.ID, false, false);


				});

				marker.addListener("mouseout",()=>{
						mapController.handleInfoWindows(marker.ID, false, true);
						if ( marker.focused ) {
							marker.setAnimation( null );
						}

				});
				marker.addListener("click",()=>{
						window.location.href = marker.permalink;
				});


				// Add event listener
				marker.addListener("click", () => {

					if ( ! marker.goes ) {

						// Calculate the zoom
						let maxZoom = 17;
						let zoom = mapObject.getZoom();

						if ( zoom < maxZoom ) {
							let difference = maxZoom - zoom;
							let newZoom = maxZoom - difference * 0.6;
							mapObject.setZoom(newZoom);
						}

						mapObject.panTo( marker.getPosition() );

					}

				  });

				this.items.push( marker );
			}
		});

		// Remove markers to be removed
		this.items = this.items.filter(item=>{
			if ( itemsToRemove.includes( item.ID ) ) {
				item.setMap(null);
			} else {
				return item;
			}
		});

		// Center and zoom accordingly to new items
		let positions = this.items.map(i=>i.position);


		//  Create a new viewpoint bound
		var bounds = new google.maps.LatLngBounds ();
		//  Go through each...
		for (let pos of positions ) {
  			//  And increase the bounds to take this point
  			bounds.extend ( pos );
		}
		//  Fit these bounds to the map
		// this.map.panToBounds( bounds );
		this.map.fitBounds( bounds );

		this.map.panTo( bounds.getCenter() );

	}

	// close all infopanels
	handleInfoWindows(id=false, focus = true, revert = false){
		let mapObject = this.map;
		this.items.map(item=>{

			// If it should revert, activate the focused one
			if ( revert ) {

				if ( item.focused ) {
					item.bubble.open( mapObject, item );
				} else {
					item.bubble.close( mapObject, item );
				}

			} else {

				if ( item.ID === id ) {
					item.bubble.open( mapObject, item );
				} else {
					item.bubble.close( mapObject, item );
				}

				if ( focus ) {
					if ( item.ID === id ) {
						item.focused = true;
						// let content = item.marker_taxonomies_text;
						// item.bubble.setContent(content);
						// item.setTitle("Zobrazit podrobnosti");
						setTimeout(function(){
							// item.goes = true;
						},500);
					} else {
						item.focused = false;
						// item.bubble.setContent( item.name );
						item.setTitle("Přiblížit");
						item.goes = false;
					}
				}

			}


		});
	}

	deleteItem( ID ){
		this.items = this.items.filter(item=>{
			if ( item.ID == ID ) {
				item.setMap(null);
			} else {
				return item;
			}
		});
	}

	addMarker( item ){

		let marker = new google.maps.Marker(item);

		this.items.push();
	}

}
