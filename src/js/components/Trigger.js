const triggerActiveClass = "gdm-f--item__active";
const triggerAvailableClass = "gdm-f--item__available";
const triggerDisabledClass = "gdm-f--item__disabled";


/**
 * A single trigger
 */
export default class Trigger {

	constructor(controller, $el, scope, value, active = false){

		// States
		this.active = active;
		this.available = true;

		// Filtering properties
		this.scope = scope;
		this.value = parseInt(value);
		this.name = window.gdm[controller.id]["filters"][this.scope]["terms"][this.value]["name"];

		// Set properties
		this.element = $el;
		this.controller = controller;

		// Bind events
		this.element.on("click", (e,l) => {
			this.handleClick();
		});
	}

	getScope(){
		return this.controller.scopes[this.scope];
	}

	handleClick(){

		this.controller.toggleFilter( this )

	}

	/**
	 * Set the item activity
	 * @param {boolean} state
	 */
	setActivity( state = false ){
		this.active = state;
	}

	/**
	 * Set the item availability
	 * @param {boolean} state
	 */
	setAvailability( state = false ){
		this.available = state;
		if ( state == false ) {
			// this.active = false;
		}
	}

	/**
	 * Perform HTML updates based on variables
	 */
	update(){

		// Project activity into classes and attribute
		if ( this.active ) {
			this.element
				.addClass(triggerActiveClass)
				.attr("data-active",true);
		} else {
			this.element
				.removeClass(triggerActiveClass)
				.attr("data-active",false);
		}

		// Project availability into classes
		if ( this.available ) {
			this.element
				.addClass(triggerAvailableClass)
				.removeClass(triggerDisabledClass);
		} else {
			this.element
				.removeClass(triggerAvailableClass)
				.addClass(triggerDisabledClass);
		}

	}

}
