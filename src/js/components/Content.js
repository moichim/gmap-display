const contentItemActiveClass = "gdm-c--item__active";

export default class Content {
	constructor( controller, $el ){
		this.element = $el;
		this.controller = controller;
		this.id = parseInt( $el.attr("data-id") );
		this.active = false;
	}

	update() {
		if ( this.active ) {
			this.element.addClass( contentItemActiveClass );
		} else {
			this.element.removeClass( contentItemActiveClass );
		}
	}
}
