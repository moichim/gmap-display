// import $ from "jquery";

import Controller from "./Controller";


/**
 * The main manager of GDM
 */
export default class Manager {

	constructor(){

		this.controllers = [];

		// Validate inputs
		if ( window.hasOwnProperty('gdm') ) {

			let ctrls = [];

			Object.keys(window.gdm).forEach(function(key,index) {

				// Initialise the controller for each scope
				let controller = new Controller( key );
				window.gdm[key]["controller"] = controller;
				ctrls.push( controller );

			});

			this.controllers = ctrls;
		}

	}

}










