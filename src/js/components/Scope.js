const scopeActiveClass = "gdm-f--scope__active";
const scopeExpandedClass = "gdm-f--scope__expanded";
const controllerExpandedClass = "gdm__has-active-dropdown";

export default class Scope {

	constructor( controller, $el ){

		// The controller
		this.controller = controller;

		// The scope main element
		this.element = $el;

		// The scope name
		this.scope = $el.attr("data-scope");

		// Placeholder element
		this.placeholder = $el.find(".gdm-f--scope--placeholder");

		// Placeholder default state
		this.valueDefault = window.gdm[ this.controller.id ]["filters"][this.scope]["label"];

		// Placeholder current state
		this.value = this.valueDefault;



		// Referencies to child trigger objects
		this.filters = this.controller.triggers.filter(t=>{
			if (t.scope == this.scope) {
				return t.value;
			}
		});

		// Expandiness state
		this.expanded = false;

		// States & Events for dropdown filters
		if ( $el.hasClass( "gdm-f--scope__display-dropdown" ) ) {

			// Is dwopdown state
			this.is_dropdown = true;

			/**
			 * EVENT > Placeholder > click
			 *
			 * Click placeholder = collapse
			 */
			this.placeholder.on("click",()=>{
				if ( this.expanded ) {
					this.expanded = false;
				} else {
					this.expanded = true;
				}
				this.touchController();
				this.update();
			});



			/**
			 * EVENT > document > click
			 *
			 * Check if the click was outside of the filter
			 */

			// ID of the scope container
			let idScope = "#" + this.element.attr("id");
			// ID of the entire filter element
			let idFilter = "#gdm__F__" + this.controller.id.substring( 4 );
			// Reference to this scope
			let scp = this;

			// The event itself
			$(document).click(function(event) {

				var $target = $(event.target);

				// Check clicking outside of the filter closes the filter
				if(!$target.closest(idScope).length &&
				$(idScope).is(":visible")) {

				  scp.expanded = false;
				  scp.update();

				}

				// Clicking outside touches the controller
				if(!$target.closest(idFilter).length ) {

				  scp.touchController();

				}
			  });

		}

		// States & Events for inline filters
		else {
			this.is_dropdown = false;
		}

	}

	/**
	 * Set global "expandiness" class to  the root element
	 *
	 * If any scope is expanded, the controller root element should have expandiness class.
	 */
	touchController(){
		if ( this.expanded ) {
			this.controller.root.addClass( controllerExpandedClass );
		} else {
			this.controller.root.removeClass( controllerExpandedClass );
		}
	}

	/**
	 * Updates the scope HTML based on
	 * - global active filters (placeholder content, scope active class)
	 * - this scope expandiness (scope expanded/collapsed class)
	 */
	update(){

		// Get the new value of the placeholder
		// (based on active filters)
		let newVal = [];
		for ( let f of this.controller.activeFilters ) {

			for ( let ft of this.filters ) {
				if ( f == ft.value ) {
					newVal.push( this.controller.getFilterName(f) );
				}
			}

		}

		// Finalize the placeholder value
		// and apply the scope activness class
		if ( newVal.length > 0 ) {
			this.value = newVal.join(", ");
			this.element.addClass(scopeActiveClass);
		} else {
			this.value = this.valueDefault;
			this.element.removeClass(scopeActiveClass);
		}

		// Push the new placeholder value
		this.placeholder.find(".gdm-f--scope--placeholder--text").html( this.value );


		// Apply expandiness classes
		if ( this.expanded ) {

			this.element.addClass( scopeExpandedClass );

		} else {

			this.element.removeClass( scopeExpandedClass );

		}
	}

}
