import Trigger from "./Trigger";
import Scope from "./Scope";
import Content from "./Content";
import Map from "./Map";

const contentItemSelector = ".gdm-c--item";

/**
 * Controller of a particular instance of the GDM
 */
export default class Controller {

	constructor( id ){

		// Constant properties
		this.id = id;
		this.root = $("#"+id);

		// The main data
		this.content = window.gdm[id].data;
		this.has_map = window.gdm[id].has_map;
		this.map = false;

		// Find triggers
		let triggers = [];
		let controller = this;
		this.root.find(".gdm-f--item").each(function(){

			triggers.push(new Trigger(controller, $(this), $(this).attr("data-scope"), $(this).attr("data-value")));
		});
		this.triggers = triggers;

		// Find scopes
		let scopes = {};
		this.root.find(".gdm-f--scope").each(function(){

			let scope = $(this).attr("data-scope");

			scopes[ scope ] = new Scope( controller, $(this) );

		});


		this.scopes = scopes;

		// Create content components
		let list = [];
		this.root.find(contentItemSelector).each(function(){
			list.push( new Content( controller, $(this) ) );
		});
		this.list = list;

		// States
		this.activeFilters = [];
		this.availableFilters = [];
		this.activeContent = [];

		// Reset filter element
		this.reset = false;
		let reset = this.root.find(".gdm-f--group__reset");

		if ( reset ) {

			let controller = this;

			reset.on("click", function(){
				controller.activeFilters = [];
				controller.activeContent = Object.getOwnPropertyNames( controller.content ).map( item => {

					return parseInt( item );

				} );
				controller.updateAllComponents();
				controller.root.removeClass("gdm__has-active-dropdown");
			});


		}

		// Map expander
		this.expander = this.root.find(".gdm-f--group__map");

		if ( this.expander.length > 0 ) {

			let controller = this;

			this.expander.on("click",function(){
				if ( controller.root.hasClass("gdm__map-collapsed") ) {
					controller.root.removeClass("gdm__map-collapsed");
					controller.root.addClass("gdm__map-expanded");
				} else {
					controller.root.removeClass("gdm__map-expanded");
					controller.root.addClass("gdm__map-collapsed");
				}
			});
		}



	}

	// Initialise the map (if possible)
	initMap(){
		if ( this.has_map ) {

			let mapElement = this.root.find(".gdm-m--itself");

			if ( mapElement.length > 0 ) {
				this.map = new Map( this, mapElement[0] );
			}

			let vec = this;
			setTimeout( function(){

				let markers = [];
				let activeItems = [];

				// If there are no active filters, return all items
				Object.keys( vec.content ).forEach( function( key, index ){

					let item = vec.content[key];

					markers.push( item );

					activeItems.push( key );

				} );

				vec.map.updateItems( markers );
				vec.activeContent = activeItems;


			}, 500);

		}
	}

	getScopesContainers(){
		let o = {};
		for ( let scope in this.scopes ) {
			o[scope] = [];
		}
		return o;
	}

	debug( input, label = undefined ){
		if ( debugMode ) {

			if ( label != undefined ) {
				console.log( label );
			}
			console.log( input );
		}
	}

	/**
	 * Process handling of a single filter
	 *
	 * @param {Trigger} filter
	 */
	toggleFilter( filter ){

		// Proceed only when the filter is available
		if ( filter.available ) {

			// Buffer of new filters data
			let tmpF = [...this.activeFilters];

			// If the filter is active, remove it from buffers
			if ( ! tmpF.includes( filter.value ) ) {
				tmpF.push( filter.value );
			} else {
				let index = tmpF.indexOf( filter.value );

				tmpF.splice(index, 1);
			}

			// Iterate the content and queue all entries
			// that has all filters from the buffer;
			let tmpActiveContent = [];
			let tmpActiveContentTermsCount = {};

			for ( let itemID of Object.getOwnPropertyNames( this.content ) ) {

				let item = this.content[itemID];

				// If the item has any taxonomies...
				if ( item.filters.length > 0 ) {

					// ... check if all terms from the buffer above are present
					let itemHasAllActiveFilters = true;

					for ( let activeTerm of tmpF ) {

						if ( ! item.filters.includes( activeTerm ) ) {
							itemHasAllActiveFilters = false;
						}

					}

					// If item has all filters, add its values
					// to buffers for further activation
					if ( itemHasAllActiveFilters ) {

						// Buffer of active items
						tmpActiveContent.push( item.ID );

						// Buffer of active filters
						for ( let term of item.filters ) {
							if ( tmpActiveContentTermsCount.hasOwnProperty( term ) ) {
								tmpActiveContentTermsCount[term]++;
							} else {
								tmpActiveContentTermsCount[term] = 1;
							}
						}

					}
				}

			}

			// Set buffers as new values
			this.activeFilters = tmpF;
			this.availableFilters = Object.getOwnPropertyNames( tmpActiveContentTermsCount ).map(i=>{
				return parseInt(i);
			});
			this.activeContent = tmpActiveContent;

			// If no filter is selected, set default values
			if ( this.activeFilters.length == 0 || this.activeContent.length == 0 ) {



				// Available filters are all
				this.availableFilters = this.triggers.map(i=>{
					return i.value;
				});

				// Available content is all
				this.activeContent = Object.getOwnPropertyNames( this.content ).map(i=>{
					return parseInt(i);
				});

			}

			// Once the render is completed, update the entire data
			this.updateAllComponents();

		}

	}


	/**
	 * Update all components within this container
	 *
	 * This method updates mostly states of elements. The classes itself should be updated by particular update() methods.
	 */
	updateAllComponents(){

		// If the filter has any active content, add class to the main element
		if ( this.activeFilters.length == 0 ) {
			this.root.removeClass("gdm__has-active-filter");
		} else {
			this.root.addClass("gdm__has-active-filter");
		}


		// Filters

		// Update triggers & filters
		for ( let trigger of this.triggers ) {

			// Trigger activity
			if ( this.activeFilters.includes( trigger.value ) ) {
				trigger.setActivity( true ) ;
			} else {
				trigger.setActivity( false );
			}

			// Trigger availability
			// If there are some active filters,
			// disable those, who are not available
			if ( this.activeFilters.length != 0 ) {
				if ( this.availableFilters.includes( trigger.value ) ) {
					trigger.setAvailability( true );
				} else {
					trigger.setAvailability( false );
				}
			}
			// If there are no active filters, enable all filters
			else {
				trigger.setAvailability( true );
			}

			// Update trigger classes
			trigger.update();

		}

		// Content

		// Content elements and their states
		let activeContent = this.activeContent;
		// No active items means filters off
		if ( this.activeContent.length == 0 ) {
			this.list.map(item=>{
				item.active = true;
				item.update();
			});
		}
		// If there are active content items
		else {
			if ( this.list.length > 0 ) {
				this.list.map(item=>{
					if ( activeContent.includes( item.id ) ) {
						item.active = true;
					} else {
						item.active = false;
					}
					item.update();

				},this.controller);
			}
		}

		// The map itself
		if ( this.has_map && this.map != false ) {

			let markers = [];

			// If there are no active filters, return all items
			if ( this.activeContent.length == 0 ) {
				for ( let item of this.content ) {
					markers.push(item);
				}
			}

			// If there are some activer filters return only active ones
			else {
				for ( let item of this.activeContent ) {
					markers.push( this.content[item] );
				}
			}

			this.map.updateItems( markers );

		}



		// Scopes
		Object.getOwnPropertyNames( this.scopes ).map(sc=>{
			this.scopes[sc].update();
		});


	}


	getFilterName( id ){
		let filter = false;
		this.triggers.map(t=>{
			if ( t.value === id ) {
				filter = t.name;
			}
		});
		return filter;
	}
}
