/**!
 * Google Maps Display
 *
 * @author Jan Jáchim <jachim5@gmail.com>
 * @license MIT
 *
 */

import Manager from "./components/Manager";
import MapBase from "./components/MapBase";


/**
 * Manager for listing
 */
$(document).ready(function(){

	window.manager = new Manager();

	if (window.manager.controllers.length > 0) {

		// Set initial state to all controllers
		window.manager.controllers.map(ctrl=>{
			ctrl.updateAllComponents();
		});

		// Load the Gmap API key
		let klic = false;
		Object.keys(window.gdm).forEach(function(key, index){
			klic = window.gdm[key]["key"];
		});

		// Load the Gmap script

		// Create the script tag, set the appropriate attributes
		var script = document.createElement('script');
		script.src = 'https://maps.googleapis.com/maps/api/js?key='+klic+'&callback=initMap';
		script.defer = true;

		// Attach your callback function to the `window` object
		window.initMap = function() {
			// JS API is loaded and available
			initMap();
		};

		// Append the 'script' element to 'head'
		document.head.appendChild(script);
	}
});

/**
 * Trigger the map once the API is loaded
 */
function initMap(){
	if (window.hasOwnProperty("gdm") ) {
		if ( window.hasOwnProperty("manager") ) {
			Object.keys(window.gdm).forEach(function(key, index){
				let instance = window.gdm[key]["controller"];
				if ( instance.has_map ) {
					instance.initMap();
				}
			});
		}
	}
}

$(document).ready(function(){

	let single = $( ".gdm-single" );

	if ( single.length > 0 ) {

		single.each(function(){


			// Load data from DATA ATTR
			let lat = parseFloat( $(this).attr("data-lat") );
			let lon = parseFloat( $(this).attr("data-lon") );
			let icon = $(this).attr("data-icon");
			let address = $(this).attr("data-address");
			let klic = $(this).attr("data-key");

			let zoom = $(this).attr("data-zoom");

			// Map
			let mapElement = $(this);


			// Load the script
			// Load the Gmap script

			// Create the script tag, set the appropriate attributes
			var script = document.createElement('script');
			script.src = 'https://maps.googleapis.com/maps/api/js?key='+klic+'&callback=initMapSingle';
			script.defer = true;

			// Attach your callback function to the `window` object
			window.initMapSingle = function() {

				// JS API is loaded and available
				let map = new MapBase( mapElement[0], lat, lon, 15.0 );

				let hasMarker = false;

				// Add listener to init
				map.map.addListener("idle", () => {

					if ( ! hasMarker ) {

						hasMarker = true;

						let options = {
							position: {
								lat: lat,
								lng: lon
							},
							icon: icon,
							map: map.map
						};

						let marker = new google.maps.Marker( options );

						let contentString = address;

						const infowindow = new google.maps.InfoWindow({
							content: contentString,
							maxWidth: 200,
						});

						infowindow.open(map, marker);

						marker.addListener("click", () => {
							infowindow.open(map, marker);
						});

						// If has zoom attribute, add it
						if (zoom) {
							map.map.setZoom(parseInt(zoom));
						}

					}

				});

			};

			// Append the 'script' element to 'head'
			document.head.appendChild(script);


		});

	}

});



