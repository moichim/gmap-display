Google Map Display
===

A Wordpress plugin that allows you to display data on an interactive Google Map and filter them by taxonomies. Main features:

- define a custom post type for map data items
- define any number of taxonomies for the purpose of filtering
- optionally, you can add custom icons for:
	- individual entries on the map
	- individual taxonomies in the filter
- insert the map anywhere you want using `gdm()` or `[gdm]` with a lot of parameters

How it works
---------------

Maps and filtering is a common task. Every implementation is slightly different and therefore there is no universal "black box" solution.

This plugin provides an opinionated starter for filtering and displaying map data in Wordpress. It enables you to define your structure of map data and give your own names to it. The JS and CSS wil work on any configuration.

The plugin is not "production ready". Configuration and final steps are intentionally left empty for your own implementation.

TBD
---
- `GDM_Manager::load_data()` is slow because it loads lot of custom fields and custom taxonomies for each item. In the future, all those post data may be cached into one custom field upon saving. Therefore, each post can be loaded in one request.
- **Implement templates for rendering** The HTML output is defined in `GDM_Manager::render_filters` and `GDM_Manager::render_content()`. Which is not good for integrating in themes. If I managed to render the HTML output of filters and lists through templates, it would be much better, because we could use data from item custom fields defined outside of this plugin.

Multiple instancs of `gdm()` on a single page
---

Both JS and CSS supports multiple `gdm()` shortcodes on the same page. Frontpage assets are loaded only if a `gdm()` shortcode is present.

Styling
---

The plugin comes with a raw default style. But you will surely need to override it with your own styles. For that purpose:

- CSS is written in BEM conventions
- in each `gdm()`, you can specify which styles to include:
	- `layout` = minimalist CSS providing functionality
	- `all` = layout + default colouring
	- `none` = no CSS from this plugin shall shall be used

Configuration of the plugin
===

So far, everything is set in `gmap-display.php` as constants (`define()`). In the future, everything can be moved into database through settings page.

Each option is documented in the config file. Main settings are:
- custom post type properties
- filtering
	- allows you to define any number of taxonomies used for filtering
	- taxonomies can display as inline or dropdown filters
	- taxonomies may have own icons


Displaying maps using `gdm()`
===

In your templates, you can render a custom list of GDM items using the `gdm( $args, $print )` function. The content can be customised passing these arguments:
```php
$args = array(
	"id" => "instance_id", // namespacing of JS & CSS
	"label" => "Filter label:", // filter name
	"disable_filters" => false,
	"has_map" => true,
	"collapsible_map" => true,
	"expand_map" => true, // default state
	"classes" => "",
	"filters" => "taxonomy_1, taxonomy_2", // Keys from GDM_FILTERING
	"css" => "all", // all / layout / none
	"contains" => "filter, map, tagline, list" // ordering of items
);
gdm( $args );
```

Every argument is optional. Default values are:
```php
$args = array(
	"id" => GDM_Manager::generateRandomString(5),
	"label" => GDM_LABEL,
	"disable_filters" => false,
	"disable_map" => false,
	"expand_map" => true,
	"collapsible_map" => true,
	"classes" => "",
	"filters" => implode(",",array_keys(GDM_FILTERING)),
	"css" => "all",
	"contains" => "filter, map, tagline, list"
);
```

Detail of one item - `single-gdm.php`
===

Although the CPT can be named enyhow in `gmap-display.php`, the template in the custom theme must allways named `single-gdm.php`.

The plugin allways looks for the following templates (in order):

- `single-gdm.php` in the active theme
- `single-gdm.php` in the plugin's `templates/` directory (only when `GDM_PLUGIN_TEMPLATES` is `true`)
- `single.php` in the active theme
- `singular.php` in the active theme
- `index.php` in the active theme





Development
---------------

Notes regarding the docker development environment:

- Install `Docker` and `docker-compose`.
- Create a folder somewhere and copy `docker-compose.yml` from this repository into it.
- Navigate into that folder and hit `docker-compose up`. The command will create a network of containers containing a webserver, database and `wp-cli` container.
- Finally, copy this theme into the `./wordpress/wp-content/themes/` directory.

### WP Cli

- Navigate into the container and run `docker-compose run --rm cli bash`.






