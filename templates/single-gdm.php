<?php
/**
 * The template for displaying single post defail
 *
 * The CPT is defined in gmap-display.php. Name of the post type can be varied,
 * but the plugin insists that the template shopuld allways have the name
 * single-gdm.php.
 *
 * Functions like get_field() or get_post_meta() can be run here.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @see gmap-display.php
 * @package Gmap_Display
 */

get_header();
?>

<div class="">

	<h1><?php the_title();?></h1>
	<p>This is the post's detail.</p>

</div>

<?php
get_footer();
