<?php
/**
 * Template lookup for the CPT detail
 *
 *
 * @package Gmap_Display
 */


function gdm_plugin_templates( $template ) {

	// When displaying GDM_CPT_DISPLAY detail
    if (is_singular(GDM_CPT_SLUG)) {

		$lookup = array(
			get_template_directory() . "/single-gdm.php",
			GDM_PLUGIN_PATH . "templates/single-gdm.php",
			get_template_directory() . "/single.php",
			get_template_directory() . "/singular.php",
			get_template_directory() . "/index.php"
		);

		foreach ( $lookup as $file ) {

			if ( file_exists( $file ) ) {

				if ( $file != GDM_PLUGIN_PATH . "templates/single-gdm.php" ) {
					return $file;
				} else {
					if ( GDM_PLUGIN_TEMPLATES ) {
						return $file;
					}
				}

			}


		}
	}
	// In all other cases return the default template
	else {
		return $template;
	}
}

add_filter('template_include' , 'gdm_plugin_templates', 99);



/**
 * Template tags
 */
function is_gdm() {
	$post_type = get_post_type();
	if ( $post_type == GDM_CPT_SLUG ) {
		return true;
	} else {
		return false;
	}
}

/**
 * GDM Charge Post
 */
function gdm_charge_post( $post ){

	if ( gettype( $post ) == "WP_Post" ) {

	} else if ( gettype( $post ) == "integer" ) {
		$post = get_post( $post );
		if ( !gettype( $post ) == "WP_Post" ) { $post = false; }
	} else {
		$post = get_post();
		if ( gettype( $post ) == "WP_Post" ) {
			$post = false;
		}
	}

	return $post;

}


/**
 * The item thumbnail
 */
function gdm_detail_thumbnail_url( $post = NULL ){

	$post = gdm_charge_post( $post );

	if ( $thumb = get_the_post_thumbnail_url( $post->ID ) ) {
		return $thumb;
	} else {
		return GDM_PLUGIN_URL . "gdm-thumbnail-default.svg";
	}

	return get_the_post_thumbnail_url( $post->ID );

}

/**
 * The item item
 */
function gdm_detail_title( $post = NULL ) {

	$post = gdm_charge_post( $post );

	return get_the_title( $post );

}

if ( !function_exists( "gdm_superify" ) ) {

	function gdm_superify( $input ){

		$output = str_replace("m3","m<sup>3</sup>",$input);

		$output = str_replace("m2","m<sup>2</sup>",$output);

		return $output;

	}

}
