<?php

class GDM_Manager {


	public $available_filters;
	public $has_map;
	public $expand_map;
	private $map_collapsible;
	public $has_css;
	public $filters;
	public $content;
	public $render;
	private $contains;
	private $label;
	private $json;
	private $classes;
	private $id;
	private $user_classes;

	/** @var WP_Query $query the post query */
	private $query;

	public function __construct(){

		// Set default values for further overrides
		$this->available_filters = array_keys( GDM_FILTERING );
		$this->has_map = true;
		$this->expand_map = false;
		$this->map_collapsible = true;
		$this->css = "all";
		$this->label = GDM_LABEL;
		$this->filters = array();
		$this->content = array();
		$this->render = false;
		$this->json = array();
		$this->classes = array();
		$this->id = $this->generateRandomString(7);
		$this->userClasses = "";
		$this->contains = array("filter","tagline", "map","list");
		$this->query = null;

	}

	public function debug($d){
		print("<pre>");
		print_r($d);
		print("</pre>");
	}

	public function setLabel($label): void
	{
		$this->label = $label;
	}

	public function setCssState( $state ): void
	{
		$this->css = $state;
	}

	public function setID( $id ): void
	{
		$this->id = $id;
	}

	public function setAvailableFilters( $filters ): void
	{

		$f = $this->strip_string_list( $filters );

		$this->available_filters = array_intersect( $f, $this->available_filters );

	}

	public function setMapAvailability( $state ): void
	{
		$this->has_map = $state;
	}

	public function setMapExpandiness( $state ): void
	{
		$this->expand_map = $state;
	}

	public function setMapExpandability( $state ) {
		$this->map_collapsible = $state;
		if ( $state == false ) {
			$this->expand_map = true;
		}
	}

	public function setClasses( $classes ){
		$this->userClasses = $classes;
	}

	public function setContains( $order ) {

		$o = array();

		// If a string is provided, parse it into array
		if ( gettype( $order ) == "string" ) {
			$o = $this->strip_string_list( $order );
		}
		// if an array is provided, proceed
		else if ( gettype( $order ) == "array" ) {
			$o = $order;
		}

		// set valid items from the order
		$this->contains = array_intersect( $o, array( "filter", "map", "list", "tagline" ) );

		// Finally, disable the map when not present in this variable
		if ( ! in_array("map", $this->contains) ) {
			$this->has_map = false;
		}

	}

	public function load() {
		$this->load_filters_data();
		$this->load_posts_data();
	}

	private function strip_string_list( $string ){
		$array = explode( "," , $string );

		for ( $i=0; $i < count($array); $i++) {
			$array[$i] = str_replace(" ", "", $array[$i]);
		}

		return $array;
	}



	private function load_filters_data(){

		foreach ( $this->available_filters as $key ) {

			$tax_name = GDM_CPT_SLUG . "_" . $key;

			$terms = get_terms( $tax_name, array(
				'hide_empty' => true,
			) );

			if ( count($terms) > 0 ) {

				$this->filters[ $key ] = array(
					"label" => GDM_FILTERING[$key]["label"],
					"terms" => array(),
					"inline" => GDM_FILTERING[$key]["displays_inline"],
					"has_icon" => GDM_FILTERING[$key]["icon"]
				);

				foreach ( $terms as $term ) {
					$this->filters[ $key ]["terms"][ $term->term_id ] = $term;

					if ( $this->filters[ $key ]["has_icon"] ) {
						$this->filters[ $key ]["terms"][ $term->term_id ]->icon = get_field( "icon", $term );
					}
				}

			}



		}

		// $this->debug( $this->filters );


	}

	private function load_posts_data(){

		$args = array(
			"post_type" => GDM_CPT_SLUG,
			"post_status" => "publish",
			"posts_per_page" => 0
		);

		$q = new WP_Query( $args );

		$this->query = $q;

		if ( $q->have_posts() ) {
			foreach ( $q->posts as $pd ) {

				// Get post data
				$p = array(
					"ID" => $pd->ID,
					"post_title" => $pd->post_title,
					"post_content" => $pd->post_content,
					"permalink" => get_post_permalink( $pd->ID ),
					"thumb" => get_the_post_thumbnail_url( $pd->ID, "medium" ),
					"taxonomies" => array(),
					"filters" => array()
				);

				// Provide link to default post thumbnail
				if ( empty( $p["thumb"] ) ) {
					$p["thumb"] = GDM_THUMB_DEFAULT;
				}


				// Get post metadata
				$meta = get_post_meta( $pd->ID );

				if ( array_key_exists( "gdm_lat_def", $meta ) ) {
					$p[ "lat" ] = $meta["gdm_lat_def"][0];
				}
				if ( array_key_exists( "gdm_lon_def", $meta ) ) {
					$p[ "lon" ] = $meta["gdm_lon_def"][0];
				}

				if ( count( $meta ) > 0 ) {
					foreach ( $meta as $key => $field ) {
						if ( in_array( $key, GDM_CPT_QUERY_FIELDS ) ) {
							$p[ $key ] = $field[0];
						}
					}
				}

				// Get post taxonomies
				foreach ( array_keys( GDM_FILTERING ) as $taxonomy ) {

					$terms = get_the_terms( $pd->ID, GDM_CPT_SLUG . '_' . $taxonomy );

					$p["taxonomies"][$taxonomy] = array();

					if ( gettype( $terms ) == "array" ) {
						if ( count($terms) > 0 ) {
							$tax_array = array();

							foreach ( $terms as $t ) {
								$tax_array[] = $t->term_id;
								$p["filters"][] = $t->term_id;
							}

							$p["taxonomies"][$taxonomy] = $tax_array;

						}
					}

				}

				$p["marker_taxonomies"] = array();

				// Attach post taxonomies for the purpose of the marker
				foreach ( GDM_CPT_QUERY_TAXONOMIES as $index => $taxonomy ){

					$terms = get_the_terms( $pd->ID, GDM_CPT_SLUG ."_".$taxonomy );

					$p["marker_taxonomies"][$index] = array();

					if ( gettype( $terms ) == "array" ) {
						if ( count( $terms ) > 0 ) {



							$list_of_terms = array();

							foreach ( $terms as $term ) {

								$p[ "marker_taxonomies" ][ $index ][] = gdm_superify( $term->name );

								// array_push( $list_of_terms, gdm_superify( $term->name ) );

								// $this->debug( $term->name );

							}

							if ( count( $list_of_terms ) > 0 ) {
								$p["marker_taxonomies"][0] = $list_of_terms;
							}



						}
					}


				}


				// Load the item icon for google maps
				$p["icon"] = NULL;

				// First, look for the post icon
				if ( array_key_exists("gdm_icon_image_def", $meta) ) {
					if ( !empty( $meta["gdm_icon_image_def"][0] ) ) {
						$p["icon"] = wp_get_attachment_image_url( $meta["gdm_icon_image_def"][0]);
					}
				}

				// Second, look for the icon in taxonomy
				if ( empty( $p["icon"] ) && GDM_ICONS ) {
					$icons = get_the_terms( $pd->ID, GDM_CPT_SLUG . "_icons" );
					if ( gettype( $icons ) == "array" ) {
						if ( count($icons) > 0 ) {
							$tmi = get_field( "icon", $icons[0]);
							if ( $tmi ) {
								$p["icon"] = $tmi;
							}
						}
					}

				}


				// Lastly, look for the default icon
				if ( empty( $p["icon"] ) ) {
					$p["icon"] = GDM_PLUGIN_URL . "gdm-marker-default-static.svg";
				}



				// Load flags, if defined
				if ( GDM_FLAGS ) {

					// Load flags
					$flags = get_the_terms( $pd->ID, GDM_CPT_SLUG . "_flags" );
					$fl = false;

					// Validate flags
					if ( gettype( $flags ) == "array" ) {
						if ( count( $flags ) > 0 ) {
							$fl = true;
						}
					}

					// Assign flags
					if ( $fl ) {
						$p["has_flags"] = true;
						$p["flags"] = $flags;
					} else {
						$p["has_flags"] = false;
					}

					// $this->debug( $flags );

				} else {

					$p["has_lags"] = false;

				}



				$this->content[ $pd->ID ] = $p;

			}

		}

		// Assign the query to the object

	}

	private function get_default_params_from_URL(){}

	private function assamle_json(){
		$this->json["filters"] = $this->filters;
		$this->json["data"] = $this->content;
		$this->json["has_map"] = $this->has_map;
		$this->json["map_expanded"] = $this->expand_map;
		$this->json["has_filters"] = count($this->filters) > 0 ? true : false;
		$this->json["key"] = GDM_KEY;
		$this->json["id"] = $this->id;
	}

	private function assamble_classes(){

		// If the map has filters
		if ( count($this->filters) > 0 && in_array( "filter", $this->contains ) ) {
			$this->classes[] = "has-filters";
		} else {
			$this->classes[] = "no-filters";
		}

		// If the filter has map
		if ( ! $this->has_map || ! in_array("map",$this->contains)) {
			$this->classes[] = "no-map";
		} else {
			$this->classes[] = "has-map";

			// Map expandiness
			if ( $this->expand_map ) {
				$this->classes[] = "map-expanded";
			} else {
				$this->classes[] = "map-collapsed";
			}

			// Map expandability
			if ( $this->map_collapsible ) {
				$this->classes[] = "map-collapsible";
			} else {
				$this->classes[] = "map-not-collapsible";
			}

		}

		if ( !empty( $this->css ) ) {
			if ( $this->css == "all" ) {
				$this->classes[] = "css-layout";
				$this->classes[] = "css-skin";
			}
			if ( $this->css == "layout" ) {
				$this->classes[] = "css-layout";
				$this->classes[] = "css-no-skin";
			}
			if ( $this->css == "none" ) {
				$this->classes[] = "css-none";
			}
		}

	}

	/**
	 * Generate a random string of a given length
	 * @param int $length default: 10
	 * @return string
	*/
	private function generateRandomString(int $length = 10): string
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/**
	 * Render the entire output
	 * @return string
	 * @todo Individual render functions should print directly. Now they use ob_
	 */
	public function render(): string
	{

		// Init the the internal buffer
		$this->render = "";

		// Assamble properties
		$this->assamble_classes();
		$classes = implode( " gdm__", $this->classes);

		// Start rendering
		$this->render .= "<div id='gdm_{$this->id}' class='gdm gdm__{$classes} {$this->userClasses}'>";

		// The JSON shall be rendered first
		$this->render_json();

		// Render parts
		foreach ( $this->contains as $part ) {
			switch( $part ){
				case "filter":
					$this->render_filter();
					break;
				case "map":
					$this->render_map();
					break;
				case "tagline":
					$this->render_tagline();
					break;
				case "list":
					$this->render_content();
			}
		}

		// Finish rendering
		$this->render .= "</div>";

		// Return the complete string
		return $this->render;
	}

	/**
	 * Print the JSON data to $this->render
	 */
	private function render_json(): void
	{
		// Assamble the JSON
		$this->assamle_json();

		ob_start();

		?>

		<script type='text/javascript'>

			if ( !window.hasOwnProperty('gdm') ){
				window.gdm = {};
			};

			window.gdm['gdm_<?= $this->id; ?>'] = <?= json_encode( $this->json ) ?>;

		</script>

		<?php

		$this->render .= ob_get_contents();
		ob_end_clean();
	}

	private function render_tagline(){

	}

	/**
	 * Prints the filter into $this->render
	 */
	private function render_filter(): void
	{


		// Start the output buffer
		ob_start();

		?>

		<div id="gdm__F__<?= $this->id; ?>" class="gdm-f">
			<div class="gdm-f--container">

				<!-- The filter label -->
				<?php if ( !empty( $l = GDM_LABEL ) ): ?>

					<div class='gdm-f--group gdm-f--group__label gdm-f--group__heading'><?= $this->label; ?></div>

				<?php endif; ?>

				<!-- The filter content -->
				<?php foreach ( $this->filters as $key => $values ): ?>

					<?php

						// Assamble variables

						// Handle icons displaying
						$has_icons = $this->filters[$key]["has_icon"];
						$has_icons_class = $has_icons ? "has-icons" : "no-icons";

						// Handle inline / dropdown logic
						$is_inline = GDM_FILTERING[$key]["displays_inline"];
						$is_inline_class = $is_inline ? "display-inline" : "display-dropdown";

					?>
						<nav id='gdm_<?= $this->id; ?>_scope_<?= $key; ?>' class='gdm-f--group gdm-f--group__taxonomy gdm-f--scope gdm-f-scope__<?= $key; ?> gdm-f--scope__<?= $has_icons_class; ?> gdm-f--scope__<?= $is_inline_class; ?>' data-scope='<?= $key; ?>' aria-labelledby='#gdm-f--scope--label__<?= $key; ?>'>

							<!-- The filter scope label -->
							<div id='gdm-f--scope--label__<?= $key; ?>' class='gdm-f--scope--label'><?=$values["label"]?></div>

							<!-- The filter scope placeholder -->
							<div class='gdm-f--scope--placeholder'><span class='gdm-f--scope--placeholder--text'><?= $values["label"]; ?></span><span class='gdm-f--scope--placeholder--image'></span></div>

							<!-- The filter scope content -->
							<ul class='gdm-f--list gdm-f--list__<?= $key; ?>'>

								<?php foreach ( $values["terms"] as $item ): ?>
									<li class='gdm-f--item' data-scope='<?= $key; ?>' data-value='<?= $item->term_id; ?>' data-active='true'>

									<?php if ( $has_icons ) : ?>
										<img class='gdm-f--item--icon' src='<?= wp_get_attachment_image_url( $item->icon ); ?>'>
									<?php endif; ?>

									<span class='gdm-f--item--name'><?= gdm_superify( $item->name ); ?></span>

									<?php if ( ! $is_inline ) : ?>
										<span class='gdm-f--item--image'></span>
									<?php endif; ?>

									</li>
								<?php endforeach; ?>

							</ul>

						</nav>

				<?php endforeach; ?>

				<!-- The map expander -->
				<?php if ( $this->has_map && $this->map_collapsible ) : ?>
				<div class='gdm-f--group gdm-f--group__map'><span class='gdm-f--group__label'>
					<!-- <img src='<?= GDM_PLUGIN_URL; ?>gdm-marker-default-static.svg'>-->
					<span class='gdm-f--maptoggle gdm-f--maptoggle_show'>zobrazit<span class='xxx'> mapu</span></span>
					<span class='gdm-f--maptoggle gdm-f--maptoggle_hide'>skrýt<span class='xxx'> mapu</span></span>
				</div>
				<?php endif; ?>

				<!-- The filter reseter -->
				<div class='gdm-f--group gdm-f--group__reset'>
					<span class='gdm-f--group__label'>reset<span class='xxx'>ovat filtr</span></span>
				</div>

			</div>
		</div>

		<?php
		// End the output buffer and assign its content to $this–>render()
		$this->render .= ob_get_contents();
		ob_end_clean();


	}


	/**
	 * Renders the map element into $this->render
	 */
	private function render_map(): void
	{
		if ( $this->has_map ) {

			ob_start();

			?>

			<div id='gdm__M__<?php $this->id; ?>' class='gdm-m'>
				<div class='gdm-m--container'>
					<div id='gdm__M__<?php $this->id; ?>__itself' class='gdm-m--itself'></div>
				</div>
			</div>

			<?php

			$this->render .= ob_get_contents();

			ob_end_clean();

		}

	}


	/**
	 * Renders the post teasers into $this->render
	 *
	 * The content of 'teaser-gdm.php' is wrapped inside of
	 * a div needed for the JS to function.
	 *
	 *
	 * @uses WP_Query
	 * @uses Output Buffer ob_
	 * @return void
	 */
	private function render_content(): void
	{

		// Start the output buffer
		ob_start();

		?>
		<div id='gdm__C__<?= $this->id;?>' class="gdm-c">
			<div class="gdm-c--container">

			<?php

				/**
				 * Render the posts query through teaser-gdm.php template
				 */

				if ( $this->query->have_posts() ):

					while( $this->query->have_posts() ):
						$this->query->the_post();

						// Print the wrapping element for the filter JS
						?>
						<div class="gdm-c--item gdm-c--item__active" data-id="<?php the_ID(); ?>">
						<?php


						// Look for 'teaser-gdm.php' in template
						if ( locate_template("teaser-gdm.php") ) {

							// If found, use it
							get_template_part( "teaser-gdm.php" );

						} else {

							// If not found, use the one that comes from this plugin
							include( plugin_dir_path(__DIR__) . "templates/teaser-gdm.php" );

						}

						// End the wrapping element
						?>
						</div>
						<?php


					endwhile;

				endif;
			?>

			</div>
		</div>

		<?php
		// Assign the printed value
		$this->render .= ob_get_contents();

		// End the output buffer
		ob_end_clean();

	}



}
