<?php
/**
 * Definitions of custom post types
 * @package Gmap_Display
*/




/**
 * Entities goes first
 */


/**
 * Main post type
 */
function gdm_post_type_definition() {

	$labels = array(
		'name'                  => GDM_CPT_NAME,
		'singular_name'         => GDM_CPT_NAME,
		'menu_name'             => GDM_CPT_NAME_PL,
		'name_admin_bar'        => GDM_CPT_NAME,
		'archives'              => GDM_CPT_NAME . " " . __( 'Archives', 'gmap_display' ),
		'attributes'            => __( 'Item Attributes', 'gmap_display' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gmap_display' ),
		'all_items'             => __( 'All Items', 'gmap_display' ),
		'add_new_item'          => __( 'Add New', 'gmap_display' ) . " " . GDM_CPT_NAME,
		'add_new'               => __( 'Add New', 'gmap_display' ),
		'new_item'              => __( 'New', 'gmap_display' ) . " " . GDM_CPT_NAME,
		'edit_item'             => __( 'Edit', 'gmap_display' ) . " " . GDM_CPT_NAME,
		'update_item'           => __( 'Update', 'gmap_display' ) . " " . GDM_CPT_NAME,
		'view_item'             => __( 'View', 'gmap_display' ) . " " . GDM_CPT_NAME,
		'view_items'            => __( 'View', 'gmap_display' ) . " " . GDM_CPT_NAME_PL,
		'search_items'          => __( 'Search', 'gmap_display' ) . " " . GDM_CPT_NAME_PL,
		'not_found'             => __( 'Not found', 'gmap_display' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gmap_display' ),
		'featured_image'        => __( 'Featured Image', 'gmap_display' ),
		'set_featured_image'    => __( 'Set featured image', 'gmap_display' ),
		'remove_featured_image' => __( 'Remove featured image', 'gmap_display' ),
		'use_featured_image'    => __( 'Use as featured image', 'gmap_display' ),
		'insert_into_item'      => __( 'Insert into item', 'gmap_display' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gmap_display' ),
		'items_list'            => __( 'Items list', 'gmap_display' ),
		'items_list_navigation' => __( 'Items list navigation', 'gmap_display' ),
		'filter_items_list'     => __( 'Filter items list', 'gmap_display' ),
	);
	$args = array(
		'label'                 => GDM_CPT_NAME,
		'description'           => __( 'Post Type Description', 'gmap_display' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', "revisions" ),
		// 'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'				=> GDM_CPT_ICON
	);
	register_post_type( GDM_CPT_SLUG, $args );

}
add_action( 'init', 'gdm_post_type_definition', 0 );

/**
 * Taxonomy for icons
 */
function gdm_icons_definition() {

	if ( GDM_ICONS ) {

		$labels = array(
			'name'                       => _x( 'Icons', 'Icons', 'gmap_display' ),
			'singular_name'              => _x( 'Icon', 'Icon', 'gmap_display' ),
			'menu_name'                  => __( 'Icons', 'gmap_display' ),
			'all_items'                  => __( 'All Icons', 'gmap_display' ),
			'parent_item'                => __( 'Parent Icon', 'gmap_display' ),
			'parent_item_colon'          => __( 'Parent Icon:', 'gmap_display' ),
			'new_item_name'              => __( 'New Icon Name', 'gmap_display' ),
			'add_new_item'               => __( 'Add New Icon', 'gmap_display' ),
			'edit_item'                  => __( 'Edit Icon', 'gmap_display' ),
			'update_item'                => __( 'Update Icon', 'gmap_display' ),
			'view_item'                  => __( 'View Icon', 'gmap_display' ),
			'separate_items_with_commas' => __( 'Separate icons with commas', 'gmap_display' ),
			'add_or_remove_items'        => __( 'Add or remove iicons', 'gmap_display' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'gmap_display' ),
			'popular_items'              => __( 'Popular Icons', 'gmap_display' ),
			'search_items'               => __( 'Search Icons', 'gmap_display' ),
			'not_found'                  => __( 'Not Found', 'gmap_display' ),
			'no_terms'                   => __( 'No icons', 'gmap_display' ),
			'items_list'                 => __( 'Icons list', 'gmap_display' ),
			'items_list_navigation'      => __( 'Icons list navigation', 'gmap_display' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		);
		register_taxonomy( GDM_CPT_SLUG . '_icons', array( GDM_CPT_SLUG ), $args );

	}

	if ( GDM_FLAGS ) {
		$labels = array(
			'name'                       => _x( 'Flags', 'Tags', 'gmap_display' ),
			'singular_name'              => _x( 'Flag', 'Tag', 'gmap_display' ),
			'menu_name'                  => __( 'Flags', 'gmap_display' ),
			'all_items'                  => __( 'All Flags', 'gmap_display' ),
			'parent_item'                => __( 'Parent Flag', 'gmap_display' ),
			'parent_item_colon'          => __( 'Parent Flag', 'gmap_display' ),
			'new_item_name'              => __( 'New Flag Name', 'gmap_display' ),
			'add_new_item'               => __( 'Add New Flag', 'gmap_display' ),
			'edit_item'                  => __( 'Edit Flag', 'gmap_display' ),
			'update_item'                => __( 'Update Flag', 'gmap_display' ),
			'view_item'                  => __( 'View Flag', 'gmap_display' ),
			'separate_items_with_commas' => __( 'Separate flags with commas', 'gmap_display' ),
			'add_or_remove_items'        => __( 'Add or remove flags', 'gmap_display' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'gmap_display' ),
			'popular_items'              => __( 'Popular Flags', 'gmap_display' ),
			'search_items'               => __( 'Search Flags', 'gmap_display' ),
			'not_found'                  => __( 'Not Found', 'gmap_display' ),
			'no_terms'                   => __( 'No icons', 'gmap_display' ),
			'items_list'                 => __( 'Flags list', 'gmap_display' ),
			'items_list_navigation'      => __( 'Flags list navigation', 'gmap_display' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => false,
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( GDM_CPT_SLUG . '_flags', array( GDM_CPT_SLUG ), $args );
	}


}
add_action( 'init', 'gdm_icons_definition', 0 );



/**
 * Filtering definitions
 */
function gdm_filters_definition(){
	foreach ( GDM_FILTERING as $key => $values ) {

		$labels = array(
			'name'                       => $values["label_pl"],
			'singular_name'              => $values["label"],
			'menu_name'                  => $values["label_pl"],
			'all_items'                  => __( 'All',"gmap_display") . " " . $values["label_pl"],
			'parent_item'                => __( 'Parent', "gmap_display") . " " . $values["label"],
			'parent_item_colon'          => __( 'Parent', "gmap_display") .":". $values["label"],
			'new_item_name'              => __( 'Name of new',"gmap_display")." ". $values["label"],
			'add_new_item'               => __( 'Add New', 'gmap_display' ),
			'edit_item'                  => __( 'Edit', 'gmap_display' )." ". $values["label"],
			'update_item'                => __( 'Update', 'gmap_display' )." ". $values["label"],
			'view_item'                  => __( 'View', 'gmap_display' )." ". $values["label"],
			'separate_items_with_commas' => __( 'Separate items with commas', 'gmap_display' ),
			'add_or_remove_items'        => __( 'Add or remove', 'gmap_display' )." ". $values["label_pl"],
			'choose_from_most_used'      => __( 'Choose from the most used', 'gmap_display' ),
			'popular_items'              => __( 'Popular', 'gmap_display' )." ". $values["label_pl"],
			'search_items'               => __( 'Search', 'gmap_display' )." ". $values["label_pl"],
			'not_found'                  => __( 'Not Found', 'gmap_display' ),
			'no_terms'                   => __( 'No items', 'gmap_display' ),
			'items_list'                 => __( 'List of', 'gmap_display' )." ". $values["label_pl"],
			'items_list_navigation'      => $values["label_pl"] . __( 'list navigation', 'gmap_display' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => $values["hierarchical"],
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		);
		register_taxonomy( GDM_CPT_SLUG . '_' . $key, array( GDM_CPT_SLUG ), $args );

	}
}

add_action( 'init', 'gdm_filters_definition', 0 );


/**
 * Fields for the CPT
 */
if( function_exists('acf_add_local_field_group') ):

	$args = array(
		'key' => 'gdm_acf_fields',
		'title' => 'Umístění na mapě',
		'fields' => array(

			array(
				'key' => 'gdm_lat',
				'label' => 'Lattitude',
				'name' => 'gdm_lat_def',
				'type' => 'text',
				'instructions' => 'Vložte zeměpisnou šířku.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'gdm_lon',
				'label' => 'Longitude',
				'name' => 'gdm_lon_def',
				'type' => 'text',
				'instructions' => 'Vložte zeměpisnou délku.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'gdm_address',
				'label' => 'Adresa',
				'name' => 'gdm_address_def',
				'type' => 'text',
				'instructions' => 'Vyplňte jednořádkovou variantu adresy.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'gdm_icon_image',
				'label' => 'Vlastní značka na mapě',
				'name' => 'gdm_icon_image_def',
				'type' => 'image',
				'instructions' => 'Obrázek by měl mít formát PNG / SVG a jeho maximální rozměry jsou ' . GDM_ICON_X . 'px a ' . GDM_ICON_Y . 'px.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '25',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => GDM_ICON_X,
				'max_height' => GDM_ICON_Y,
				'max_size' => '',
				'mime_types' => 'png, svg',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => GDM_CPT_SLUG,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	);

	// Append Icon taxonomy relation if defined
	if ( GDM_ICONS ) {
		$args["fields"][] = array(
			'key' => 'gdm_ref_icon_tax',
			'label' => 'Předdefinovaná ikona',
			'name' => 'gdm_ref_icon_tax_def',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => GDM_CPT_SLUG . '_icons',
			'field_type' => 'radio',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 1,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		);
	}

	if ( GDM_FLAGS ) {
		$args["fields"][] = array(
			'key' => 'gdm_ref_flag_tax',
			'label' => 'Štítky',
			'name' => 'gdm_ref_flag_tax_def',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => GDM_CPT_SLUG . '_flags',
			'field_type' => 'multi_select',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		);
	}

	acf_add_local_field_group( $args );

endif;


/**
 * Fields for taxonomies
 */
if( function_exists('acf_add_local_field_group') ):

	// Get the settings of taxonomies that should have icons
	$taxonomies = array();
	foreach ( GDM_FILTERING as $key => $settings ) {
		if ( $settings['icon'] == true ) {
			$taxonomies[] = GDM_CPT_SLUG . "_" . $key;
		}
	}
	if (GDM_ICONS) {
		$taxonomies[] = GDM_CPT_SLUG . "_icons";
	}

	// print_r( $taxonomies );

	// Proceed only when there are some taxonomies to proceed
	if ( count( $taxonomies ) > 0 ) {
		$args = array(
			'key' => 'gdm_group_taxonomies_icon',
			'title' => 'Ikona taxonomie',
			'fields' => array(
				array(
					'key' => 'gdm_taxonomy_icon_field',
					'label' => 'Ikona',
					'name' => 'icon',
					'type' => 'image',
					'instructions' => 'Nahrajte ikonu ve formátu PNG či SVG a o rozměrech max. 50px',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'url',
					'preview_size' => 'full',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => GDM_ICON_X,
					'max_height' => GDM_ICON_Y,
					'max_size' => '',
					'mime_types' => 'png, svg',
				),
			),
			'location' => array(
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'seamless',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => true,
			'description' => '',
		);

		// Hook the locations as OR conditions
		foreach ( $taxonomies as $taxonomy_slug ) {
			$args["location"][] = array(
				array(
					"param" => "taxonomy",
					"operator" => "==",
					"value" => $taxonomy_slug
				)
			);
		}

		// Append the metabox to taxonomies
		acf_add_local_field_group( $args );

	}



endif;







