<?php

/**
 * Register assets for further enqueuing
 */
function gdm_assets_register() {

    // Register JS
	wp_register_script( 'gdm_js', GDM_PLUGIN_URL . 'assets/gdm.min.js', array("jquery"), GDM_VERSION );

	// Register CSS
	wp_register_style( 'gdm_css', GDM_PLUGIN_URL . 'assets/gdm.min.css', "all", GDM_VERSION );

}

add_action("wp_enqueue_scripts", "gdm_assets_register");


/**
 * Enqueue the assets if the [gdm] shortcode is being used in the post content
 *
 * @link https://developer.wordpress.org/reference/functions/wp_register_script/#user-contributed-notes
 */
function gdm_assets_enqueue() {

	// global $post;

    // if ( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'gdm') ) {

		wp_enqueue_script( 'gdm_js' );
		wp_enqueue_style( "gdm_css" );

    // }
}

add_action( 'wp_enqueue_scripts', 'gdm_assets_enqueue');
