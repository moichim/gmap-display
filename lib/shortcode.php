<?php




// Add Shortcode
function gdm_shortcode( $input ) {

	// return $manager->render_filter();

	$atts = shortcode_atts( array(
        'filters' => NULL,
		'disable_filters' => NULL,
		"has_map" => NULL,
		"expand_map" => NULL,
		"id" => NULL,
		"css" => NULL,
		"label" => NULL,
		"contains" => NULL,
		"collapsible_map" => NULL
	), $input, 'gdm' );

	// Pass only those arguments that are provided
	$args = array();
	foreach ( $atts as $key => $value ) {
		if ( $value != NULL ) {

			if ( strtolower( $value) == "true" ) {
				$value = true;
			}

			else if ( strtolower( $value == "false" ) ) {
				$value = false;
			}

			$args[ $key ] = $value;
		}
	}

	// print_r( $args );

	// Call the inclusion function
	return gdm( $args, false );


}
add_shortcode( 'gdm', 'gdm_shortcode' );
