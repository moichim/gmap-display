<?php
/**
 * Inclusion function for the listing
 */

if ( ! function_exists( "gdm" ) ) {

	function gdm( $arguments = array() , $print = true ){

		// Default arguments
		$defaults = array(
			"id" => NULL,
			"label" => GDM_LABEL,
			"disable_filters" => false,
			"has_map" => true,
			"expand_map" => true,
			"collapsible_map" => true,
			"filters" => implode(",",array_keys(GDM_FILTERING)),
			"css" => "all",
			"classes" => "",
			"contains" => array(
				"filter", "tagline", "map", "list"
			),
		);

		// Merge defaults with user input
		$args = array_merge( $defaults, $arguments );

		// Initialise the class
		$manager = new GDM_Manager();

		// Set the ID
		if ( $args["id"] != NULL ) {
			$manager->setID( $args["id"] );
		}

		// Set the CSS
		$manager->setCssState( $args["css"] );

		// Set the label
		$manager->setLabel( $args["label"] );

		// Set available filters
		if ( $args["disable_filters"] ) {
			$manager->setAvailableFilters( array() );
		} else {
			$manager->setAvailableFilters( $args["filters"] );
		}

		// Map availability
		$manager->setMapAvailability( $args["has_map"] );

		// Handle map expandiness
		$manager->setMapExpandiness( $args["expand_map"] );

		// Handle map collapsibility
		$manager->setMapExpandability( $args["collapsible_map"] );

		$manager->setClasses( $args["classes"] );

		// Set order
		$manager->setContains( $args["contains"] );

		// Load the manager data
		$manager->load();

		// Render the result
		$result = $manager->render();

		// Print values if needed
		if ( $print ) {
			print $result;
		}

		return $result;

	}

}

if ( ! function_exists( "gdm_single" ) ) {

	function gdm_single(){

		$data = gdm_single_data();

		?>
			<div class="gdm-single" data-lat="<?= $data["lat"]; ?>" data-lon="<?= $data["lon"]; ?>" data-address="<?= $data["address"]; ?>" data-key="<?= GDM_KEY; ?>" data-icon="<?= $data["icon"]; ?>"></div>
		<?php

	}

	function gdm_single_data(){

		$post = gdm_single_charge_post();

		$meta = get_post_meta( $post->ID );

		$data = array(
			"lat" => false,
			"lon" => false,
			"address" => false,
			"icon" => GDM_PLUGIN_URL . "gdm-marker-default-static.svg",
		);

		$keys = array( "lat", "lon", "address" );
		foreach ( $keys as $key ) {
			$post_meta_name = "gdm_".$key."_def";
			if ( array_key_exists( $post_meta_name, $meta ) ) {
				if ( in_array( $key, array("lat", "lon") ) ) {
					$data[$key] = floatval( $meta[$post_meta_name][0] );
				} else if ($key == "address") {
					$data[$key] = $meta[$post_meta_name][0];
				}

			}
		}

		if ( array_key_exists( "gdm_icon_image_def", $meta ) ) {

			if ( $meta["gdm_icon_image_def"][0] != false ) {

				$mid = $meta["gdm_icon_image_def"][0];
				$data["icon"] = wp_get_attachment_image_url( $mid );

			}

		}

		return $data;

	}

}


if ( ! function_exists( "gdm_single_charge_post" ) ) {

	function gdm_single_charge_post(){

		$post = get_post();
		if ( $post->post_type == GDM_CPT_SLUG ) {
			return $post;
		} else {
			return false;
		}
	}

}
